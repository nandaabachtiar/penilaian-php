<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Absensi extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('siswa_model', 'siswa');
		$this->load->model('kelas_model', 'kelas');
		$this->load->model('absensi_model', 'absensi');
	}

	public function index($id_rombel='', $bulan='', $tahun='')
	{
			$data = array(
			'title'		=> 'Absensi | Apps Sekolah',
			'rombel'	=> $this->kelas->list(),
			'data'		=> $this->absensi->list(),
			'content'	=> 'admin/absensi/v_add_v2'
		);
		// die('cuk');
		$this->load->view('admin/layout/v_wrapper_v2', $data, FALSE);
	}

	public function list($id_rombel='', $bulan='', $tahun='')
	{
			$data = array(
			'title'		=> 'Absensi | Apps Sekolah',
			'rombel'	=> $this->kelas->list(),
			'id_rombel'	=> $id_rombel,
			'bulan'		=> $bulan,
			'tahun'		=> $tahun,
			'data'		=> $this->absensi->list($id_rombel, $bulan),
			'absen'		=> $this->absensi,
			'content'	=> 'admin/absensi/v_add_v2'
		);
		$this->load->view('admin/layout/v_wrapper_v2', $data, FALSE);
	}

	public function list_absen($id_siswa='', $bulan='', $tahun='')
	{
		$this->absensi->list_absen($id_siswa, $bulan, $tahun);
	}

	public function add()
	{
		$i = $this->input;
		$data = array(
			'id_siswa'       	=> $i->post('id_siswa'),
			'bulan'       		=> $i->post('i_bulan'),
			'tahun'     		=> $i->post('i_tahun'),
			'date_1'     		=> $i->post('date_1'),
			'date_2'     		=> $i->post('date_2'),
			'date_3'     		=> $i->post('date_3'),
			'date_4'     		=> $i->post('date_4'),
			'date_5'     		=> $i->post('date_5'),
			'date_6'     		=> $i->post('date_6'),
			'date_7'     		=> $i->post('date_7'),
			'date_8'     		=> $i->post('date_8'),
			'date_9'     		=> $i->post('date_9'),
			'date_10'     		=> $i->post('date_10'),
			'date_11'     		=> $i->post('date_11'),
			'date_12'     		=> $i->post('date_12'),
			'date_13'     		=> $i->post('date_13'),
			'date_14'     		=> $i->post('date_14'),
			'date_15'     		=> $i->post('date_15'),
			'date_16'     		=> $i->post('date_16'),
			'date_17'     		=> $i->post('date_17'),
			'date_18'     		=> $i->post('date_18'),
			'date_19'     		=> $i->post('date_19'),
			'date_20'     		=> $i->post('date_20'),
			'date_21'     		=> $i->post('date_21'),
			'date_22'     		=> $i->post('date_22'),
			'date_23'     		=> $i->post('date_23'),
			'date_24'     		=> $i->post('date_24'),
			'date_25'     		=> $i->post('date_25'),
			'date_26'     		=> $i->post('date_26'),
			'date_27'     		=> $i->post('date_27'),
			'date_28'     		=> $i->post('date_28'),
			'date_29'     		=> $i->post('date_29'),
			'date_30'     		=> $i->post('date_30'),
			'date_31'     		=> $i->post('date_31')
		);
		$data2 = array(
			'id_siswa'       	=> $i->post('id_siswa'),
			'bulan'       		=> $i->post('i_bulan'),
			'tahun'     		=> $i->post('i_tahun')
		);
		$this->absensi->delete($data2);
		$this->db->insert('absensi_siswa', $data);
		$this->session->set_flashdata('sukses', '<i class="fa fa-info-circle"></i> Data siswa berhasil ditambahkan!');
		redirect(base_url('admin/absensi/list'),'refresh');
	}

}

/* End of file Guru.php */
/* Location: ./application/controllers/admin/Guru.php */