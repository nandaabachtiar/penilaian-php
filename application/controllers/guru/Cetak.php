<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cetak extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

		public function __construct() {
        parent::__construct();

        $this->output->set_header('Last-Modified:' . gmdate('D,d M Y H:i:s') . 'GMT');
        $this->output->set_header('Cache-Control:no-store, no-cache, must-revalidate');
        $this->output->set_header('Cache-Control:post-check=0,pre-check=0', false);
        $this->output->set_header('Pragma: no-cache');

        $this->load->library('Pdf');
        $this->load->model('guru_model', 'guru');
        $this->load->model('siswa_model', 'siswa');
		$this->load->model('kelas_model', 'kelas');
		$this->load->model('pengetahuan_model', 'pengetahuan');
		$this->load->model('keterampilan_model', 'keterampilan');
		$this->load->model('mapel_model', 'mapel');
    }

	public function kd_pengetahuan($id_rombel = '', $id_semester = '', $id_mapel = '')
	{

		if ($this->session->userdata('jenis_ptk') == 'Wali Kelas') {
			$mapel = (object) array(
				'id_mapel' => '0'
			);
			$get_kelas = $this->pengetahuan->get_kelas($this->session->userdata('id_guru'));
		} else {
			$mapel = $this->pengetahuan->mapel($this->session->userdata('id_guru'));
			$get_kelas = (object) array(
				'id_rombel' => '0'
			);
		}
		$data = array(
			'title'		=> 'Pelaporan Nilai | Apps Sekolah',
			'rombel'	=> $this->kelas->list(),
			'mapel_list'	=> $this->pengetahuan->list_mapel(),
			'id_rombel'		=> $id_rombel,
			'get_rombel'		=> $this->kelas->detail($id_rombel),
			'id_semester'	=> $id_semester,
			'id_mapel'	=> $id_mapel,
			'guru_mapel' => $this->pengetahuan->guru_mapel($id_mapel),
			'mapel'	=> $mapel,
			'get_kelas'	=> $get_kelas,
			'data'		=> $this->pengetahuan->list($id_rombel),
			'guru'		=> $this->guru->detail_guru($this->session->userdata('id_guru')),
			'semester'	=> $this->pengetahuan->semester(),
			'pengetahuan'		=> $this->pengetahuan
		);
		// print_r($data);die();
		$this->load->view('guru/cetak/kd_pengetahuan_v2', $data, FALSE);
	}

	public function kd_keterampilan($id_rombel = '', $id_semester = '', $id_mapel = '')
    {

        if ($this->session->userdata('jenis_ptk') == 'Wali Kelas') {
            $mapel = (object) array(
                'id_mapel' => '0'
            );
            $get_kelas = $this->keterampilan->get_kelas($this->session->userdata('id_guru'));
        } else {
            $mapel = $this->keterampilan->get_idmapel($this->session->userdata('id_guru'));
            $get_kelas = (object) array(
                'id_rombel' => '0'
            );
        }
        $data = array(
            'title'     => 'Pelaporan Nilai | Apps Sekolah',
            'rombel'    => $this->kelas->list(),
            'mapel_list'    => $this->keterampilan->list_mapel(),
            'id_rombel'     => $id_rombel,
            'get_rombel'        => $this->kelas->detail($id_rombel),
            'id_semester'   => $id_semester,
            'id_mapel'  => $id_mapel,
            'guru_mapel' => $this->keterampilan->guru_mapel($id_mapel),
            'mapel' => $mapel,
            'get_kelas' => $get_kelas,
            'data'      => $this->keterampilan->list($id_rombel),
            'guru'      => $this->guru->detail_guru($this->session->userdata('id_guru')),
            'semester'  => $this->keterampilan->list_semester(),
            'keterampilan'      => $this->keterampilan
        );
        // print_r($data);die();
        $this->load->view('guru/cetak/kd_keterampilan_v2', $data, FALSE);
    }

	public function nilai_rapot($id_siswa = '', $id_semester = '')
	{

		if ($this->session->userdata('jenis_ptk') == 'Wali Kelas') {
			$mapel = (object) array(
				'id_mapel' => '0'
			);
			$get_kelas = $this->pengetahuan->get_kelas($this->session->userdata('id_guru'));
		} else {
			$mapel = $this->pengetahuan->mapel($this->session->userdata('id_guru'));
			$get_kelas = (object) array(
				'id_rombel' => '0'
			);
		}
		$data = array(
			'title'		=> 'Pelaporan Nilai | Apps Sekolah',
			// 'rombel'	=> $this->kelas->list(),
			// 'mapel_list'	=> $this->pengetahuan->list_mapel(),
			// 'id_rombel'		=> $id_rombel,
			'siswa'		=> $this->siswa->detail($id_siswa),
			'mapel_a'		=> $this->mapel->mapel_grup('1'),
			'mapel_b'		=> $this->mapel->mapel_grup('2'),
			'mapel_c'		=> $this->mapel->mapel_grup('3', '1'),
			'mapel_c_2'		=> $this->mapel->mapel_grup('3', '2'),
			'ket_mapel_a'		=> $this->mapel->mapel_grup('1'),
			'ket_mapel_b'		=> $this->mapel->mapel_grup('2'),
			'ket_mapel_c'		=> $this->mapel->mapel_grup('3', '1'),
			'ket_mapel_c_2'		=> $this->mapel->mapel_grup('3', '2'),
			'semester'	=> $this->pengetahuan->semester(),
			'id_semester'	=> $id_semester,
			'pengetahuan'		=> $this->pengetahuan,
			'keterampilan'		=> $this->keterampilan
			// 'id_mapel'	=> $id_mapel,
			// 'mapel'	=> $mapel,
			// 'get_kelas'	=> $get_kelas,
			// 'data'		=> $this->pengetahuan->list($id_rombel),
			// 'guru'		=> $this->guru->detail_guru($this->session->userdata('id_guru')),
			// 'semester'	=> $this->pengetahuan->semester(),
		);
		// print_r($data['keterampilan']);die();
		$this->load->view('guru/cetak/nilai_rapot_v2', $data, FALSE);
	}

}
