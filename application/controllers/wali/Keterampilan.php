<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Keterampilan extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('siswa_model', 'siswa');
		$this->load->model('kelas_model', 'kelas');
		$this->load->model('keterampilan_model', 'keterampilan');
	}

	public function index($id_rombel = '')
	{
		$data = array(
			'title'		=> 'Keterampilan | Apps Sekolah',
			'rombel'	=> $this->kelas->list(),
			'data'		=> $this->keterampilan->list(),
			'content'	=> 'wali/keterampilan/v_add_v2'
		);
		// die('cuk');
		$this->load->view('wali/layout/v_wrapper_v2', $data, FALSE);
	}

	public function list($id_semester = '')
	{
		$data = array(
			'title'		=> 'KD Keterampilan | Apps Sekolah',
			'rombel'	=> $this->kelas->list(),
			'semester'	=> $this->keterampilan->list_semester(),
			'id_semester'	=> $id_semester,
			'mapel_list'	=> $this->keterampilan->list_mapel(),
			'guru'	=> $this->keterampilan->get_idmapel($this->session->userdata('id_guru')),
			'wali'	=> $this->keterampilan->get_siswa($this->session->userdata('id_wali')),
			'keterampilan'		=> $this->keterampilan,
			'content'	=> 'wali/keterampilan/v_add_v2'
		);
		$this->load->view('wali/layout/v_wrapper_v2', $data, FALSE);
	}

	public function list_keterampilan($id_siswa = '', $id_mapel = '', $id_semester = '')
	{
		$this->keterampilan->list_keterampilan($id_siswa, $id_mapel, $id_semester);
	}

	public function print_list($id_rombel = '', $id_semester = '', $id_mapel = '')
	{
		if ($this->session->userdata('jenis_ptk') == 'Wali Kelas') {
			$mapel = (object) array(
				'id_mapel' => '0'
			);
			$get_kelas = $this->keterampilan->get_kelas($this->session->userdata('id_guru'));
		} else {
			$mapel = $this->keterampilan->get_idmapel($this->session->userdata('id_guru'));
			$get_kelas = (object) array(
				'id_rombel' => '0'
			);
		}
		$data = array(
			'title'		=> 'Laporan KD Keterampilan | Apps Sekolah',
			'rombel'	=> $this->kelas->list(),
			'semester'	=> $this->keterampilan->list_semester(),
			'mapel'	=> $this->keterampilan->list_mapel(),
			'id_rombel'	=> $id_rombel,
			'id_semester'	=> $id_semester,
			'id_mapel'	=> $id_mapel,
			'guru'	=> $mapel,
			'get_kelas'	=> $get_kelas,
			'data'		=> $this->keterampilan->list($id_rombel),
			'keterampilan'		=> $this->keterampilan,
			'content'	=> 'wali/keterampilan/v_print_v2'
		);
		$this->load->view('wali/layout/v_wrapper_v2', $data, FALSE);
	}

	public function add()
	{
		// die('cuks');
		$i = $this->input;
		$nilai41 = 0;
		$nilai42 = 0;
		$nilai43 = 0;
		$nilai44 = 0;
		$nilai45 = 0;
		$nilai46 = 0;
		$nilai47 = 0;

		$divider41 = 0;
		$divider42 = 0;
		$divider43 = 0;
		$divider44 = 0;
		$divider45 = 0;
		$divider46 = 0;
		$divider47 = 0;
		if ($i->post('41_1') != '') $divider41++;
		if ($i->post('41_2') != '') $divider41++;
		if ($i->post('41_3') != '') $divider41++;
		if ($i->post('41_4') != '') $divider41++;
		if ($i->post('41_5') != '') $divider41++;
		if ($divider41 != 0) {
			$nilai41 = (intval($i->post('41_1')) + intval($i->post('41_2')) + intval($i->post('41_3')) + intval($i->post('41_4')) + intval($i->post('41_5'))) / $divider41;
		}
		if ($i->post('42_1') != '') $divider42++;
		if ($i->post('42_2') != '') $divider42++;
		if ($i->post('42_3') != '') $divider42++;
		if ($i->post('42_4') != '') $divider42++;
		if ($i->post('42_5') != '') $divider42++;
		if ($divider42 != 0) {
			$nilai42 = (intval($i->post('42_1')) + intval($i->post('42_2')) + intval($i->post('42_3')) + intval($i->post('42_4')) + intval($i->post('42_5'))) / $divider42;
		}
		if ($i->post('43_1') != '') $divider43++;
		if ($i->post('43_2') != '') $divider43++;
		if ($i->post('43_3') != '') $divider43++;
		if ($i->post('43_4') != '') $divider43++;
		if ($i->post('43_5') != '') $divider43++;
		if ($divider43 != 0) {
			$nilai43 = (intval($i->post('43_1')) + intval($i->post('43_2')) + intval($i->post('43_3')) + intval($i->post('43_4')) + intval($i->post('43_5'))) / $divider43;
		}
		if ($i->post('44_1') != '') $divider44++;
		if ($i->post('44_2') != '') $divider44++;
		if ($i->post('44_3') != '') $divider44++;
		if ($i->post('44_4') != '') $divider44++;
		if ($i->post('44_5') != '') $divider44++;
		if ($divider44 != 0) {
			$nilai44 = (intval($i->post('44_1')) + intval($i->post('44_2')) + intval($i->post('44_3')) + intval($i->post('44_4')) + intval($i->post('44_5'))) / $divider44;
		}
		if ($i->post('45_1') != '') $divider45++;
		if ($i->post('45_2') != '') $divider45++;
		if ($i->post('45_3') != '') $divider45++;
		if ($i->post('45_4') != '') $divider45++;
		if ($i->post('45_5') != '') $divider45++;
		if ($divider45 != 0) {
			$nilai45 = (intval($i->post('45_1')) + intval($i->post('45_2')) + intval($i->post('45_3')) + intval($i->post('45_4')) + intval($i->post('45_5'))) / $divider45;
		}
		if ($i->post('46_1') != '') $divider46++;
		if ($i->post('46_2') != '') $divider46++;
		if ($i->post('46_3') != '') $divider46++;
		if ($i->post('46_4') != '') $divider46++;
		if ($i->post('46_5') != '') $divider46++;
		if ($divider46 != 0) {
			$nilai46 = (intval($i->post('46_1')) + intval($i->post('46_2')) + intval($i->post('46_3')) + intval($i->post('46_4')) + intval($i->post('46_5'))) / $divider46;
		}
		if ($i->post('47_1') != '') $divider47++;
		if ($i->post('47_2') != '') $divider47++;
		if ($i->post('47_3') != '') $divider47++;
		if ($i->post('47_4') != '') $divider47++;
		if ($i->post('47_5') != '') $divider47++;
		if ($divider47 != 0) {
			$nilai47 = (intval($i->post('47_1')) + intval($i->post('47_2')) + intval($i->post('47_3')) + intval($i->post('47_4')) + intval($i->post('47_5'))) / $divider47;
		}

		$divider_rapot = 0;
		if ($nilai41 != 0) $divider_rapot++;
		if ($nilai42 != 0) $divider_rapot++;
		if ($nilai43 != 0) $divider_rapot++;
		if ($nilai44 != 0) $divider_rapot++;
		if ($nilai45 != 0) $divider_rapot++;
		if ($nilai46 != 0) $divider_rapot++;
		if ($nilai47 != 0) $divider_rapot++;
		$nilai_rapot = ($nilai41 + $nilai42 + $nilai43 + $nilai44 + $nilai45 + $nilai46 + $nilai47) / $divider_rapot;

		$data = array(
			'id_siswa'       	=> $i->post('i_siswa'),
			'id_mapel'       	=> $i->post('i_mapel'),
			'id_semester'  		=> $i->post('i_semester'),
			'41_1'     			=> $i->post('41_1'),
			'41_2'     			=> $i->post('41_2'),
			'41_3'     			=> $i->post('41_3'),
			'41_4'     			=> $i->post('41_4'),
			'41_5'     			=> $i->post('41_5'),
			'42_1'     			=> $i->post('42_1'),
			'42_2'     			=> $i->post('42_2'),
			'42_3'     			=> $i->post('42_3'),
			'42_4'     			=> $i->post('42_4'),
			'42_5'     			=> $i->post('42_5'),
			'43_1'     			=> $i->post('43_1'),
			'43_2'     			=> $i->post('43_2'),
			'43_3'     			=> $i->post('43_3'),
			'43_4'     			=> $i->post('43_4'),
			'43_5'     			=> $i->post('43_5'),
			'44_1'     			=> $i->post('44_1'),
			'44_2'     			=> $i->post('44_2'),
			'44_3'     			=> $i->post('44_3'),
			'44_4'     			=> $i->post('44_4'),
			'44_5'     			=> $i->post('44_5'),
			'45_1'     			=> $i->post('45_1'),
			'45_2'     			=> $i->post('45_2'),
			'45_3'     			=> $i->post('45_3'),
			'45_4'     			=> $i->post('45_4'),
			'45_5'     			=> $i->post('45_5'),
			'46_1'     			=> $i->post('46_1'),
			'46_2'     			=> $i->post('46_2'),
			'46_3'     			=> $i->post('46_3'),
			'46_4'     			=> $i->post('46_4'),
			'46_5'     			=> $i->post('46_5'),
			'47_1'     			=> $i->post('47_1'),
			'47_2'     			=> $i->post('47_2'),
			'47_3'     			=> $i->post('47_3'),
			'47_4'     			=> $i->post('47_4'),
			'47_5'     			=> $i->post('47_5'),
			'41_'     			=> $nilai41,
			'42_'     			=> $nilai42,
			'43_'     			=> $nilai43,
			'44_'     			=> $nilai44,
			'45_'     			=> $nilai45,
			'46_'     			=> $nilai46,
			'47_'     			=> $nilai47,
			'nilai_rapot'     	=> $nilai_rapot,
			'deskripsi'    		=> $i->post('deskripsi')
		);
		$data2 = array(
			'id_siswa'       	=> $i->post('i_siswa'),
			'id_mapel'       	=> $i->post('i_mapel'),
			'id_semester'     		=> $i->post('i_semester')
		);
		$this->keterampilan->delete($data2);
		$this->db->insert('kd_keterampilan', $data);
		$this->session->set_flashdata('sukses', '<i class="fa fa-info-circle"></i> Data siswa berhasil ditambahkan!');
		redirect(base_url('wali/keterampilan/list'), 'refresh');
	}
}

/* End of file Guru.php */
/* Location: ./application/controllers/admin/Guru.php */