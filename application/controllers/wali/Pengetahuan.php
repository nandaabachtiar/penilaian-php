<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pengetahuan extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('siswa_model', 'siswa');
		$this->load->model('kelas_model', 'kelas');
		$this->load->model('pengetahuan_model', 'pengetahuan');
	}

	public function list($id_semester = '')
	{
		$data = array(
			'title'		=> 'KD Pengetahuan | Apps Sekolah',
			'rombel'	=> $this->kelas->list(),
			'id_semester'	=> $id_semester,
			'mapel_list'	=> $this->pengetahuan->list_mapel(),
			'mapel'	=> $this->pengetahuan->mapel($this->session->userdata('id_guru')),
			'wali'	=> $this->pengetahuan->get_siswa($this->session->userdata('id_wali')),
			'semester'	=> $this->pengetahuan->semester(),
			'pengetahuan'		=> $this->pengetahuan,
			'content'	=> 'wali/pengetahuan/v_add_v2'
		);
		// print_r($data);die();
		$this->load->view('wali/layout/v_wrapper_v2', $data, FALSE);
	}

	public function list_nilai($id_siswa = '', $id_mapel = '', $id_semester = '')
	{
		$this->pengetahuan->list_nilai($id_siswa, $id_mapel, $id_semester);
	}

	public function print_list($id_rombel = '', $id_semester = '', $id_mapel = '')
	{
		if ($this->session->userdata('jenis_ptk') == 'Wali Kelas') {
			$mapel = (object) array(
				'id_mapel' => '0'
			);
			$get_kelas = $this->pengetahuan->get_kelas($this->session->userdata('id_guru'));
		} else {
			$mapel = $this->pengetahuan->mapel($this->session->userdata('id_guru'));
			$get_kelas = (object) array(
				'id_rombel' => '0'
			);
		}
		$data = array(
			'title'		=> 'KD Pengetahuan | Apps Sekolah',
			'rombel'	=> $this->kelas->list(),
			'mapel_list'	=> $this->pengetahuan->list_mapel(),
			'id_rombel'		=> $id_rombel,
			'id_semester'	=> $id_semester,
			'id_mapel'	=> $id_mapel,
			'mapel'	=> $mapel,
			'get_kelas'	=> $get_kelas,
			'data'		=> $this->pengetahuan->list($id_rombel),
			'semester'	=> $this->pengetahuan->semester(),
			'pengetahuan'		=> $this->pengetahuan,
			'content'	=> 'wali/pengetahuan/v_print_v2'
		);
		// print_r($data);die();
		$this->load->view('wali/layout/v_wrapper_v2', $data, FALSE);
	}

	public function add()
	{
		$i = $this->input;
		$temp1 = 0;
		if ($i->post('31_TL') != '') $temp1++;
		if ($i->post('31_PN') != '') $temp1++;
		if ($i->post('31_LS') != '') $temp1++;
		if ($i->post('31_PTS') != '') $temp1++;
		if ($temp1 != '0') $kd1 = (intval($i->post('31_TL')) + intval($i->post('31_PN')) + intval($i->post('31_LS')) + intval($i->post('31_PTS'))) / intval($temp1);
		$temp2 = 0;
		if ($i->post('32_TL') != '') $temp2++;
		if ($i->post('32_PN') != '') $temp2++;
		if ($i->post('32_LS') != '') $temp2++;
		if ($i->post('32_PTS') != '') $temp2++;
		if ($temp2 != '0') $kd2 = (intval($i->post('32_TL')) + intval($i->post('32_PN')) + intval($i->post('32_LS')) + intval($i->post('32_PTS'))) / intval($temp2);
		$temp3 = 0;
		if ($i->post('33_TL') != '') $temp3++;
		if ($i->post('33_PN') != '') $temp3++;
		if ($i->post('33_LS') != '') $temp3++;
		if ($i->post('33_PTS') != '') $temp3++;
		if ($temp3 != '0') $kd3 = (intval($i->post('33_TL')) + intval($i->post('33_PN')) + intval($i->post('33_LS')) + intval($i->post('33_PTS'))) / intval($temp3);
		$temp4 = 0;
		if ($i->post('34_TL') != '') $temp4++;
		if ($i->post('34_PN') != '') $temp4++;
		if ($i->post('34_LS') != '') $temp4++;
		if ($i->post('34_PTS') != '') $temp4++;
		if ($temp4 != '0') $kd4 = (intval($i->post('34_TL')) + intval($i->post('34_PN')) + intval($i->post('34_LS')) + intval($i->post('34_PTS'))) / intval($temp4);
		$temp5 = 0;
		if ($i->post('35_TL') != '') $temp5++;
		if ($i->post('35_PN') != '') $temp5++;
		if ($i->post('35_LS') != '') $temp5++;
		if ($i->post('35_PTS') != '') $temp5++;
		if ($temp5 != '0') $kd5 = (intval($i->post('35_TL')) + intval($i->post('35_PN')) + intval($i->post('35_LS')) + intval($i->post('35_PTS'))) / intval($temp5);
		$temp6 = 0;
		if ($i->post('36_TL') != '') $temp6++;
		if ($i->post('36_PN') != '') $temp6++;
		if ($i->post('36_LS') != '') $temp6++;
		if ($i->post('36_PTS') != '') $temp6++;
		if ($temp6 != '0') $kd6 = (intval($i->post('36_TL')) + intval($i->post('36_PN')) + intval($i->post('36_LS')) + intval($i->post('36_PTS'))) / intval($temp6);
		$temp7 = 0;
		if ($i->post('37_TL') != '') $temp7++;
		if ($i->post('37_PN') != '') $temp7++;
		if ($i->post('37_LS') != '') $temp7++;
		if ($i->post('37_PTS') != '') $temp7++;
		if ($temp7 != '0') $kd7 = (intval($i->post('37_TL')) + intval($i->post('37_PN')) + intval($i->post('37_LS')) + intval($i->post('37_PTS'))) / intval($temp7);
		$temp_rapot = 0;
		if ($i->post('31_PAS') != '') $temp_rapot++;
		if ($i->post('32_PAS') != '') $temp_rapot++;
		if ($i->post('33_PAS') != '') $temp_rapot++;
		if ($i->post('34_PAS') != '') $temp_rapot++;
		if ($i->post('35_PAS') != '') $temp_rapot++;
		if ($i->post('36_PAS') != '') $temp_rapot++;
		if ($i->post('37_PAS') != '') $temp_rapot++;
		if ($temp_rapot != '0') $nilai_rapot = (intval($i->post('31_PAS')) + intval($i->post('32_PAS')) + intval($i->post('33_PAS')) + intval($i->post('34_PAS')) + intval($i->post('35_PAS')) + intval($i->post('36_PAS')) + intval($i->post('37_PAS'))) / intval($temp_rapot);
		// print_r($kd1.'-'.$kd2.'-'.$kd3.'-'.$kd4.'-'.$kd5.'-'.$kd6.'-'.$kd7.'-'.$nilai_rapot);die();
		$data = array(
			'id_siswa'       	=> $i->post('id_siswa'),
			'id_mapel'       	=> $i->post('id_mapel'),
			'id_semester'      	=> $i->post('id_semester'),
			'31_TL'     		=> $i->post('31_TL'),
			'32_TL'     		=> $i->post('32_TL'),
			'33_TL'     		=> $i->post('33_TL'),
			'34_TL'     		=> $i->post('34_TL'),
			'35_TL'     		=> $i->post('35_TL'),
			'36_TL'     		=> $i->post('36_TL'),
			'37_TL'     		=> $i->post('37_TL'),
			'31_PN'     		=> $i->post('31_PN'),
			'32_PN'     		=> $i->post('32_PN'),
			'33_PN'     		=> $i->post('33_PN'),
			'34_PN'     		=> $i->post('34_PN'),
			'35_PN'     		=> $i->post('35_PN'),
			'36_PN'     		=> $i->post('36_PN'),
			'37_PN'     		=> $i->post('37_PN'),
			'31_LS'     		=> $i->post('31_LS'),
			'32_LS'     		=> $i->post('32_LS'),
			'33_LS'     		=> $i->post('33_LS'),
			'34_LS'     		=> $i->post('34_LS'),
			'35_LS'     		=> $i->post('35_LS'),
			'36_LS'     		=> $i->post('36_LS'),
			'37_LS'     		=> $i->post('37_LS'),
			'31_PTS'     		=> $i->post('31_PTS'),
			'32_PTS'     		=> $i->post('32_PTS'),
			'33_PTS'     		=> $i->post('33_PTS'),
			'34_PTS'     		=> $i->post('34_PTS'),
			'35_PTS'     		=> $i->post('35_PTS'),
			'36_PTS'     		=> $i->post('36_PTS'),
			'37_PTS'     		=> $i->post('37_PTS'),
			'31_PAS'     		=> $i->post('31_PAS'),
			'32_PAS'     		=> $i->post('32_PAS'),
			'33_PAS'     		=> $i->post('33_PAS'),
			'34_PAS'     		=> $i->post('34_PAS'),
			'35_PAS'     		=> $i->post('35_PAS'),
			'36_PAS'     		=> $i->post('36_PAS'),
			'37_PAS'     		=> $i->post('37_PAS'),
			'31_'     		=> $kd1,
			'32_'     		=> $kd2,
			'33_'     		=> $kd3,
			'34_'     		=> $kd4,
			'35_'     		=> $kd5,
			'36_'     		=> $kd6,
			'37_'     		=> $kd7,
			'nilai_rapot'	=> $nilai_rapot,
			'deskripsi'		=> $i->post('deskripsi')
		);
		$data2 = array(
			'id_siswa'       	=> $i->post('id_siswa'),
			'id_mapel'       	=> $i->post('id_mapel'),
			'id_semester'       	=> $i->post('id_semester')
		);
		$this->pengetahuan->delete($data2);
		$this->db->insert('kd_pengetahuan', $data);
		$this->session->set_flashdata('sukses', '<i class="fa fa-info-circle"></i> Data siswa berhasil ditambahkan!');
		redirect(base_url('wali/pengetahuan/list'), 'refresh');
	}
}

/* End of file Guru.php */
/* Location: ./application/controllers/admin/Guru.php */