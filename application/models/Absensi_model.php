<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Absensi_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}	

	public function list($id_rombel='', $bulan='', $tahun='')
	{
		$this->db->select('*');
		$this->db->from('siswa');
		$this->db->join('kelas', 'siswa.id_kelas = kelas.id_kelas', 'left');
		$this->db->join('rombel', 'siswa.id_rombel = rombel.id_rombel', 'left');
		$this->db->where('siswa.id_rombel', $id_rombel);
		$this->db->order_by('nama_siswa', 'asc');
		$query = $this->db->get();
		return $query->result();
	}

	public function list_absen($id_siswa='', $bulan='', $tahun='')
	{
		$this->db->select('*');
		$this->db->from('absensi_siswa');
		$this->db->where('id_siswa', $id_siswa);
		$this->db->where('bulan', $bulan);
		$this->db->where('tahun', $tahun);
		$query = $this->db->get();	
		return $query->row();
	}

	public function delete($data)
	{
		$this->db->where('id_siswa', $data['id_siswa'])->where('bulan', $data['bulan'])->where('tahun', $data['tahun']);
		$this->db->delete('absensi_siswa');
	}
}

/* End of file Admin_model.php */
/* Location: ./application/models/Admin_model.php */