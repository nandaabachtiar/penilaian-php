<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Keterampilan_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}	

	public function list($id_rombel='')
	{
		$this->db->select('*');
		$this->db->from('siswa');
		$this->db->join('kelas', 'siswa.id_kelas = kelas.id_kelas', 'left');
		$this->db->join('rombel', 'siswa.id_rombel = rombel.id_rombel', 'left');
		$this->db->where('siswa.id_rombel', $id_rombel);
		$this->db->order_by('nama_siswa', 'ssc');
		$query = $this->db->get();
		return $query->result();
	}

	public function list_semester()
	{
		$this->db->select('*');
		$this->db->from('semester');
		$query = $this->db->get();
		return $query->result(); 
	}

	public function list_nilai($id_siswa='', $id_mapel='', $id_semester='')
	{
		$this->db->select('*');
		$this->db->from('kd_keterampilan');
		$this->db->where('id_siswa', $id_siswa);
		$this->db->where('id_mapel', $id_mapel);
		$this->db->where('id_semester', $id_semester);
		$query = $this->db->get();	
		return $query->row();
	}

	public function guru_mapel($id_mapel)
	{
		$this->db->select('*');
		$this->db->from('mapel');
		$this->db->join('guru', 'mapel.id_guru = guru.id_guru', 'left');
		$this->db->where('id_mapel', $id_mapel);
		$query = $this->db->get();
		return $query->row();
	}

	public function list_mapel()
	{
		$this->db->select('*');
		$this->db->from('mapel');
		$this->db->join('kelompok_mapel', 'mapel.id_kelompok_mapel = kelompok_mapel.id_kelompok_mapel', 'left');
		$query = $this->db->get();
		return $query->result(); 
	}

	public function get_kelas($id_guru)
	{
		$this->db->select('*');
		$this->db->from('rombel');
		$this->db->where('id_guru', $id_guru);
		$query = $this->db->get();
		return $query->row();
	}

	public function list_keterampilan($id_siswa='', $id_semester='', $id_mapel='')
	{
		// print_r($id_mapel);die();
		$this->db->select('*');
		$this->db->from('kd_keterampilan');
		$this->db->where('id_siswa', $id_siswa);
		$this->db->where('id_mapel', $id_mapel);
		$this->db->where('id_semester', $id_semester);
		$query = $this->db->get();	
		// print_r($query->row());die();
		return $query->row();
	}

	public function get_idmapel($id_guru='')
	{
		$this->db->select('*');
		$this->db->from('mapel');
		$this->db->where('id_guru', $id_guru);
		$query = $this->db->get();	
		return $query->row();
	}

	public function get_siswa($id_wali='')
	{
		$this->db->select('*');
		$this->db->from('wali');
		$this->db->join('siswa', 'siswa.id_siswa = wali.id_siswa', 'left');
		$this->db->where('id_wali', $id_wali);
		$query = $this->db->get();	
		return $query->row();
	}

	public function detail_siswa($id_siswa='')
	{
		$this->db->select('*');
		$this->db->from('siswa');
		$this->db->where('id_siswa', $id_siswa);
		$query = $this->db->get();	
		return $query->row();
	}

	public function delete($data)
	{
		$this->db->where('id_siswa', $data['id_siswa'])->where('id_mapel', $data['id_mapel'])->where('id_semester', $data['id_semester']);
		$this->db->delete('kd_keterampilan');
	}
}

/* End of file Admin_model.php */
/* Location: ./application/models/Admin_model.php */