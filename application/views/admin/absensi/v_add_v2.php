<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
	<!-- BEGIN: Left Aside -->
	<!-- END: Left Aside -->
	<div class="m-grid__item m-grid__item--fluid m-wrapper">
		<div class="m-content">
			<!-- BEGIN: Subheader -->
			<div class="m-subheader ">
				<div class="d-flex align-items-center">
					<div class="mr-auto">
						<h3 class="m-subheader__title m-subheader__title--separator">
							Absensi
						</h3>
						<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
							<li class="m-nav__item m-nav__item--home">
								<a href="#" class="m-nav__link m-nav__link--icon">
									<i class="m-nav__link-icon la la-home"></i>
								</a>
							</li>
							<li class="m-nav__separator">
								-
							</li>
							<li class="m-nav__item">
								<a href="" class="m-nav__link">
									<span class="m-nav__link-text">
										Dashboard
									</span>
								</a>
							</li>
							<li class="m-nav__separator">
								-
							</li>
							<li class="m-nav__item">
								<a href="" class="m-nav__link">
									<span class="m-nav__link-text">
										Data Master
									</span>
								</a>
							</li>
							<li class="m-nav__separator">
								-
							</li>
							<li class="m-nav__item">
								<a href="" class="m-nav__link">
									<span class="m-nav__link-text">
										Absensi
									</span>
								</a>
							</li>
							
						</ul>
					</div>
				</div>
			</div>
			<!-- END: Subheader -->
			<div class="m-content">
				<div class="m-portlet m-portlet--mobile">
					<div class="m-portlet__body">
						<!--begin: Search Form -->
						<div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
							<div class="row align-items-center">
								<div class="col-xl-8 order-2 order-xl-1">
									<div class="form-group m-form__group row align-items-center">
										<div class="col-md-4">
											<div class="col-xl-4 order-1 order-xl-2 m--align-right">
												<a href="<?= base_url() ?>admin/guru" class="btn btn-warning m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
													<span>
														<i class="fa fa-arrow-left"></i>
														<span>
															Kembali
														</span>
													</span>
												</a>
												<div class="m-separator m-separator--dashed d-xl-none"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="m-portlet__head">
							<div class="m-portlet__head-caption">
								<div class="m-portlet__head-title">
									<h3 class="m-portlet__head-text">
										Absensi
									</h3>
								</div>
							</div>
						</div>
						<br>
						<div>
							<?php  
							echo validation_errors('<div class="alert alert-danger alert-slide-up">','</div>');
							?>
							
								<div class="row">
									<div class="form-group col-md-6">
										<label for="">Pilih Bulan</label>
										<select class="form-control" name="bulan" id="bulan">
											<option value="">- Pilih Bulan -</option>
											<option value="1" <?php if ($bulan=="1") {echo "selected";} ?>>Januari</option>
											<option value="2" <?php if ($bulan=="2") {echo "selected";} ?>>Februari</option>
											<option value="3" <?php if ($bulan=="3") {echo "selected";} ?>>Maret</option>
											<option value="4" <?php if ($bulan=="4") {echo "selected";} ?>>April</option>
											<option value="5" <?php if ($bulan=="5") {echo "selected";} ?>>Mei</option>
											<option value="6" <?php if ($bulan=="6") {echo "selected";} ?>>Juni</option>
											<option value="7" <?php if ($bulan=="7") {echo "selected";} ?>>Juli</option>
											<option value="8" <?php if ($bulan=="8") {echo "selected";} ?>>Agusutus</option>
											<option value="9" <?php if ($bulan=="9") {echo "selected";} ?>>September</option>
											<option value="10" <?php if ($bulan=="10") {echo "selected";} ?>>Oktober</option>
											<option value="11" <?php if ($bulan=="11") {echo "selected";} ?>>November</option>
											<option value="12" <?php if ($bulan=="12") {echo "selected";} ?>>Desember</option>
										</select>
									</div>
									<div class="form-group col-md-6">
									</div>
									<div class="form-group col-md-6">
										<label for="">Pilih Tahun</label>
										<select class="form-control" id="tahun" name="tahun">
											<option value="">- Pilih Tahun -</option>
											<?php for($i=2019;$i<=date('Y');$i++){ ?>
											<option value="<?php echo $i ?>" <?php if ($tahun==$i) {echo "selected";} ?>><?php echo $i ?></option>
											<?php } ?>
										</select>
									</div>
									<div class="form-group col-md-6">
									</div>
									<div class="form-group col-md-6">
										<label for="">Rombel</label>
										<select name="id_rombel" id="" class="form-control" required="" onchange="filter($(this).val(), $('#bulan').val(), $('#tahun').val())">
											<option value="">- Pilih Rombel -</option>
											<?php foreach ($rombel as $key => $value): ?>
												<option value="<?= $value->id_rombel ?>" <?php if ($id_rombel==$value->id_rombel) {echo "selected";} ?>><?= $value->rombel ?></option>
											<?php endforeach ?>
										</select>
									</div>
									
									<div class="form-group col-md-12" style="overflow-x: auto;">
										<table border="1" width="100%" id="table_absensi" class="table table-bordered">
											 <thead>
												<tr>
													<th rowspan="2">Nama</th>
													<th colspan="31">Tanggal</th>
													<th rowspan="2">Aksi</th>
												</tr>
												<tr>
													<?php for($i=1;$i<=31;$i++){ ?>
														<th><?= $i ?></th>
													<?php } ?>
												</tr>
											</thead>
											<tbody>
													<?php foreach ($data as $key => $value): ?>
													<form action="<?= base_url() ?>admin/absensi/add/" method="post" enctype="multipart/form-data">
													<input type="hidden" name="i_bulan" value="<?php echo $bulan ?>">
													<input type="hidden" name="i_tahun" value="<?php echo $tahun ?>">
													<tr>
														<td>
															<?= $value->nama_siswa ?>
															<input type="hidden" name="id_siswa" value="<?= $value->id_siswa ?>">
															
														</td>
														<?php for($i=1;$i<=31;$i++){ $nanda = (array) $absen->list_absen($value->id_siswa, $bulan, $tahun);?>
															
															<td>
																<select name="date_<?= $i ?>">
																	<?php if(count($nanda) > 0){ ?>
																		<option value="">Pilih</option>
																		<option value="Y" <?php if ($nanda['date_'.$i]=="Y") {echo "selected";} ?>>Masuk</option>
																		<option value="I" <?php if ($nanda['date_'.$i]=="I") {echo "selected";} ?>>Izin</option>
																		<option value="S" <?php if ($nanda['date_'.$i]=="S") {echo "selected";} ?>>Sakit</option>
																		<option value="A" <?php if ($nanda['date_'.$i]=="A") {echo "selected";} ?>>Alpa</option>
																	<?php }else{ ?>
																		<option value="">Pilih</option>
																		<option value="Y">Masuk</option>
																		<option value="I">Izin</option>
																		<option value="S">Sakit</option>
																		<option value="A">Alpa</option>
																<?php } ?>
																</select>
															</td>
														<?php } ?>
														<td>
															<button type="submit" class="btn btn-accent">Simpan</button>
														</td>
													</tr>
													</form>
													<?php endforeach ?>
											</tbody> 
										</table>
									</div>
									
								</div>
								<!-- <button type="submit" class="btn btn-accent"><i class="fa fa-save"></i> Simpan</button>
								<button type="reset" class="btn btn-danger"><i class="fa fa-refresh"></i> Reset</button> -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	function filter(id_rombel, bulan, tahun){
		var url = '<?php echo base_url() ?>admin/absensi/list/'+id_rombel+'/'+bulan+'/'+tahun;
		location.href = url;
	}
</script>