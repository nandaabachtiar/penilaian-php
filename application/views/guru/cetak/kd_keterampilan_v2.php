<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {

    function __construct()
    {
        parent::__construct();
    }

    //Page header
    public function Header() {
        // Logo
        $image_file = 'assets/images/logo_ipctpk.png';
        $this->Image($image_file, 155, 10, 40, '', 'png', '', 'T', false, 300, '', false, false, 0, false, false, false);
        
         $bMargin = $this->getBreakMargin();
        // get current auto-page-break mode
        $auto_page_break = $this->AutoPageBreak;
        // disable auto-page-break
        $this->SetAutoPageBreak(false, 0);
        // restore auto-page-break status
        $this->SetAutoPageBreak($auto_page_break, $bMargin);
        // set the starting point for the page content
        $this->setPageMark();
    }

    // Page footer
    public function Footer() {
        $this->SetFont('times', 'B', 8);
        $this->SetY(-60);
        $this->SetX(0);
        $this->Cell(200, 100, '', 0, false, 'R', 0, '', 0, false, 'T', 'M');
    }
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('fahmiganz');
$pdf->SetTitle('PELAPORAN NILAI');
$pdf->SetSubject('KD KETERAMPILAN');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, 40, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
// $pdf->SetFont('times', 'BI', 12);
$pdf->SetFont('times');

// add a page
// $pdf->AddPage();
$pdf->AddPage('L', 'F4');
// $pdf->Cell(0, 0, 'A4 LANDSCAPE', 1, 1, 'C');
// print_r($guru_mapel->nip);die();
$satuan_pendidikan  = 'SMA TAMAN MADYA JETIS YOGYAKARTA';
$kepsek             = 'ERMAYANTI, M.Pd';
$nip_kepsek         = 'NIP. 19750507 200012 002';
$mapel              = $guru_mapel->nama_mapel;
$nama_guru_mapel    = $guru_mapel->nama_guru;
$nip_guru_mapel     = 'NIP. '.$guru_mapel->nip;
$tahun_ajaran       = $semester[0]->tahun_pelajaran;
$semester_          = $semester[0]->semester;
$rombel_            = $get_rombel->rombel;

ob_start();
// print_r($data);die();
$page1 = '
<style>
h2{
    text-align: center;
}
</style>
<h2>PENGOLAHAN DAN PELAPORAN HASIL PENILAIAN KETERAMPILAN</h2>
<table border="0" style="font-size: 12px;">
  <tbody>
    <tr>
      <td>
        <table border="0">
          <tbody>
            <tr>
              <td width="40%">Satuan Pendidikan</td>
              <td width="5%">:</td>
              <td width="55%">'.$satuan_pendidikan.'</td>
            </tr>
          </tbody>
        </table>
      </td>
      <td>
        <table border="0">
          <tbody>
            <tr>
              <td width="40%">Thn. Pelajaran</td>
              <td width="5%">:</td>
              <td width="55%">'.$tahun_ajaran.'</td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <tr>
      <td>
        <table border="0">
          <tbody>
            <tr>
              <td width="40%">Mata Pelajaran</td>
              <td width="5%">:</td>
              <td width="55%">'.$mapel.'</td>
            </tr>
          </tbody>
        </table>
      </td>
      <td>
        <table border="0">
          <tbody>
            <tr>
              <td width="40%">Semester / KKM</td>
              <td width="5%">:</td>
              <td width="55%">'.$semester_.'</td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <tr>
      <td>
        <table border="0">
          <tbody>
            <tr>
              <td width="40%">Kelas / Program</td>
              <td width="5%">:</td>
              <td width="55%">'.$rombel_.'</td>
            </tr>
          </tbody>
        </table>
      </td>
      <td>
        <table border="0">
          <tbody>
            <tr>
              <td width="40%">Guru Mapel</td>
              <td width="5%">:</td>
              <td width="55%">'.$nama_guru_mapel.'</td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>
<br>
<br>
<table border="1" style="font-size:12px;">
  <tbody>
    <tr>
      <td rowspan="2" width="3%">NO</td>
      <td rowspan="2" width="10%">NIS</td>
      <td rowspan="2" width="18%">NAMA PESERTA DIDIK</td>
      <td rowspan="2" width="3%">L/P</td>
      <td colspan="5" width="13.2%">KD 4.1</td>
      <td colspan="5" width="13.2%">KD 4.2</td>
      <td colspan="5" width="13.2%">KD 4.3</td>
      <td colspan="5" width="13.2%">KD 4.4</td>
      <td colspan="5" width="13.2%">KD 4.5</td>
    </tr>
    <tr>
      <td>1</td>
      <td>2</td>
      <td>3</td>
      <td>4</td>
      <td>5</td>
      <td>1</td>
      <td>2</td>
      <td>3</td>
      <td>4</td>
      <td>5</td>
      <td>1</td>
      <td>2</td>
      <td>3</td>
      <td>4</td>
      <td>5</td>
      <td>1</td>
      <td>2</td>
      <td>3</td>
      <td>4</td>
      <td>5</td>
      <td>1</td>
      <td>2</td>
      <td>3</td>
      <td>4</td>
      <td>5</td>
    </tr>
  </tbody>
  <tbody>';
$no_page1 = 1;
for($i = 0; $i < count($data); $i++){
  $nilai = (array) $keterampilan->list_nilai($data[$i]->id_siswa, $id_mapel, $id_semester);
  $page1 .= '
    <tr>
      <td>'.$no_page1.'</td>
      <td>'.$data[$i]->nis.'</td>
      <td>'.$data[$i]->nama_siswa.'</td>
      <td>'.$data[$i]->jk_siswa.'</td>
      <td>'.$nilai['41_1'].'</td>
      <td>'.$nilai['41_2'].'</td>
      <td>'.$nilai['41_3'].'</td>
      <td>'.$nilai['41_4'].'</td>
      <td>'.$nilai['41_5'].'</td>
      <td>'.$nilai['42_1'].'</td>
      <td>'.$nilai['42_2'].'</td>
      <td>'.$nilai['42_3'].'</td>
      <td>'.$nilai['42_4'].'</td>
      <td>'.$nilai['42_5'].'</td>
      <td>'.$nilai['43_1'].'</td>
      <td>'.$nilai['43_2'].'</td>
      <td>'.$nilai['43_3'].'</td>
      <td>'.$nilai['43_4'].'</td>
      <td>'.$nilai['43_5'].'</td>
      <td>'.$nilai['44_1'].'</td>
      <td>'.$nilai['44_2'].'</td>
      <td>'.$nilai['44_3'].'</td>
      <td>'.$nilai['44_4'].'</td>
      <td>'.$nilai['44_5'].'</td>
      <td>'.$nilai['45_1'].'</td>
      <td>'.$nilai['45_2'].'</td>
      <td>'.$nilai['45_3'].'</td>
      <td>'.$nilai['45_4'].'</td>
      <td>'.$nilai['45_5'].'</td>
      <td></td>
    </tr>
  ';
  $no_page1++;
}
for($i = 0; $i < 20 - count($data); $i++){
  $page1 .= '
    <tr><td>'.$no_page1.'</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
  ';
  $no_page1++;
}
$page1 .= '
  </tbody>
</table>
<table style="font-size:12px;">
  <tbody>
    <tr>
      <td>Keterangan</td>
      <td><b>1 untuk kerja/kinerja/praktik</b></td>
      <td><b>2 produk</b></td>
      <td><b>3 proyek</b></td>
      <td><b>4 portofolio</b></td>
      <td><b>5 teknik lain /tes tertulis</b></td>
    </tr>
  </tbody>
</table>
<br>
<br>
<table border="0">
  <tbody>
    <tr>
      <td>Mengetahui,</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>Kepala Sekolah</td>
      <td></td>
      <td></td>
      <td>Guru Mata Pelajaran</td>
    </tr>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>'.$kepsek.'</td>
      <td></td>
      <td></td>
      <td>'.$nama_guru_mapel.'</td>
    </tr>
    <tr>
      <td>'.$nip_kepsek.'</td>
      <td></td>
      <td></td>
      <td>'.$nip_guru_mapel.'</td>
    </tr>
  </tbody>
</table>
';

// output the HTML content
$pdf->writeHTMLCell($w='', $h='', $x=15, $y=10, $page1, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
//Close and output PDF document
ob_end_clean();
// $pdf->SetMargins(100, 100, 100, true);
$pdf->AddPage('L', 'F4');
ob_start();
$page2 = '
<style>
h2{
    text-align: center;
    color: white;
}
</style>
<h2>PENGOLAHAN DAN PELAPORAN HASIL PENILAIAN KETERAMPILAN</h2>
<table border="0" style="color:white;">
  <tbody>
    <tr>
      <td>
        <table border="0">
          <tbody>
            <tr>
              <td width="40%">Satuan Pendidikan</td>
              <td width="5%">:</td>
              <td width="55%">sd</td>
            </tr>
          </tbody>
        </table>
      </td>
      <td>
        <table border="0">
          <tbody>
            <tr>
              <td width="40%">Thn. Pelajaran</td>
              <td width="5%">:</td>
              <td width="55%">sd</td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <tr>
      <td>
        <table border="0">
          <tbody>
            <tr>
              <td width="40%">Mata Pelajaran</td>
              <td width="5%">:</td>
              <td width="55%">sd</td>
            </tr>
          </tbody>
        </table>
      </td>
      <td>
        <table border="0">
          <tbody>
            <tr>
              <td width="40%">Semester / KKM</td>
              <td width="5%">:</td>
              <td width="55%">sd</td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <tr>
      <td>
        <table border="0">
          <tbody>
            <tr>
              <td width="40%">Kelas / Program</td>
              <td width="5%">:</td>
              <td width="55%">sd</td>
            </tr>
          </tbody>
        </table>
      </td>
      <td>
        <table border="0">
          <tbody>
            <tr>
              <td width="40%">Guru Mapel</td>
              <td width="5%">:</td>
              <td width="55%">sd</td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>
<br>
<br>
<table border="1" style="font-size:12px;">
  <tbody>
    <tr>
      <td rowspan="2" width="3%">NO</td>
      <td rowspan="2" width="10%">NIS</td>
      <td rowspan="2" width="18%">NAMA PESERTA DIDIK</td>
      <td rowspan="2" width="3%">L/P</td>
      <td colspan="5" width="13.2%">4.6</td>
      <td colspan="5" width="13.2%">4.7</td>
      <td colspan="7" width="18.48%">Nilai KD</td>
      <td rowspan="2" width="6%">NILAI RAPORT</td>
      <td rowspan="2" width="15.12%">DESKRIPSI</td>
    </tr>
    <tr>
        <td>1</td>
        <td>2</td>
        <td>3</td>
        <td>4</td>
        <td>5</td>
        <td>1</td>
        <td>2</td>
        <td>3</td>
        <td>4</td>
        <td>5</td>
        <td>4.1</td>
        <td>4.2</td>
        <td>4.3</td>
        <td>4.4</td>
        <td>4.5</td>
        <td>4.6</td>
        <td>4.7</td>
    </tr>
  </tbody>
  <tbody>';
$no_page2 = 1;
for($i = 0; $i < count($data); $i++){
  $nilai = (array) $keterampilan->list_nilai($data[$i]->id_siswa, $id_mapel, $id_semester);
  $page2 .= '
    <tr>
    <td>'.$no_page2.'</td>
    <td>'.$data[$i]->nis.'</td>
    <td>'.$data[$i]->nama_siswa.'</td>
    <td>'.$data[$i]->jk_siswa.'</td>
    <td>'.$nilai['46_1'].'</td>
    <td>'.$nilai['46_2'].'</td>
    <td>'.$nilai['46_3'].'</td>
    <td>'.$nilai['46_4'].'</td>
    <td>'.$nilai['46_5'].'</td>
    <td>'.$nilai['47_1'].'</td>
    <td>'.$nilai['47_2'].'</td>
    <td>'.$nilai['47_3'].'</td>
    <td>'.$nilai['47_4'].'</td>
    <td>'.$nilai['47_5'].'</td>
    <td>'.$nilai['41_'].'</td>
    <td>'.$nilai['42_'].'</td>
    <td>'.$nilai['43_'].'</td>
    <td>'.$nilai['44_'].'</td>
    <td>'.$nilai['45_'].'</td>
    <td>'.$nilai['46_'].'</td>
    <td>'.$nilai['47_'].'</td>
    <td>'.$nilai['nilai_rapot'].'</td>
    <td>'.$nilai['deskripsi'].'</td>
    </tr>
  ';
$no_page2++;
}
for($i = 0; $i < 20 - count($data); $i++){
  $page2 .= '
    <tr>
    <td>'.$no_page2.'</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
    </tr>
  ';
$no_page2++;
}
$page2 .= '
  </tbody>
</table>

';

// output the HTML content
$pdf->writeHTMLCell($w='', $h='', $x=15, $y=10, $page2, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);
ob_end_clean();
// ---------------------------------------------------------

$pdf->Output('KD KETERAMPILAN.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
