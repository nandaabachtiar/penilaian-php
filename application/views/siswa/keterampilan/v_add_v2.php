<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
	<!-- BEGIN: Left Aside -->
	<!-- END: Left Aside -->
	<div class="m-grid__item m-grid__item--fluid m-wrapper">
		<div class="m-content">
			<!-- BEGIN: Subheader -->
			<div class="m-subheader ">
				<div class="d-flex align-items-center">
					<div class="mr-auto">
						<h3 class="m-subheader__title m-subheader__title--separator">
							KD Keterampilan
						</h3>
						<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
							<li class="m-nav__item m-nav__item--home">
								<a href="#" class="m-nav__link m-nav__link--icon">
									<i class="m-nav__link-icon la la-home"></i>
								</a>
							</li>
							<li class="m-nav__separator">
								-
							</li>
							<li class="m-nav__item">
								<a href="" class="m-nav__link">
									<span class="m-nav__link-text">
										Dashboard
									</span>
								</a>
							</li>
							<li class="m-nav__separator">
								-
							</li>
							<li class="m-nav__item">
								<a href="" class="m-nav__link">
									<span class="m-nav__link-text">
										Data Master
									</span>
								</a>
							</li>
							<li class="m-nav__separator">
								-
							</li>
							<li class="m-nav__item">
								<a href="" class="m-nav__link">
									<span class="m-nav__link-text">
										KD Keterampilan
									</span>
								</a>
							</li>

						</ul>
					</div>
				</div>
			</div>
			<!-- END: Subheader -->
			<div class="m-content">
				<div class="m-portlet m-portlet--mobile">
					<div class="m-portlet__body">
						<!--begin: Search Form -->
						<div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
							<div class="row align-items-center">
								<div class="col-xl-8 order-2 order-xl-1">
									<div class="form-group m-form__group row align-items-center">
										<div class="col-md-4">
											<div class="col-xl-4 order-1 order-xl-2 m--align-right">
												<a href="<?= base_url() ?>siswa/keterampilan" class="btn btn-warning m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
													<span>
														<i class="fa fa-arrow-left"></i>
														<span>
															Kembali
														</span>
													</span>
												</a>
												<div class="m-separator m-separator--dashed d-xl-none"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="m-portlet__head">
							<div class="m-portlet__head-caption">
								<div class="m-portlet__head-title">
									<h3 class="m-portlet__head-text">
										KD Keterampilan (<?= $siswa->nama_siswa ?>)
									</h3>
								</div>
							</div>
						</div>
						<br>
						<div>
							<?php
							echo validation_errors('<div class="alert alert-danger alert-slide-up">', '</div>');
							?>

							<div class="row">
								<div class="form-group col-md-6">
									<label for="">Semester</label>
									<select name="id_semester" id="id_semester" class="form-control" required="" onchange="filter()">
										<option value="">- Pilih Semester -</option>
										<?php foreach ($semester as $key => $value) : ?>
											<option value="<?= $value->id_semester ?>" <?php if ($id_semester == $value->id_semester) {
																							echo "selected";
																						} ?>><?= $value->semester ?> <?= $value->tahun_pelajaran ?></option>
										<?php endforeach ?>
									</select>
								</div>

								<div class="form-group col-md-12" style="overflow-x: auto;">
									<table border="1" width="100%" id="table_kd_keterampilan" class="table table-bordered">
										<thead>
											<tr>
												<th rowspan="2" width="20px">NAMA MATA PELAJARAN</th>
												<th colspan="5">KD 4.1</th>
												<th colspan="5">KD 4.2</th>
												<th colspan="5">KD 4.3</th>
												<th colspan="5">KD 4.4</th>
												<th colspan="5">KD 4.5</th>
												<th colspan="5">KD 4.6</th>
												<th colspan="5">KD 4.7</th>
												<th colspan="7">Nilai KD</th>
												<th rowspan="2">Nilai Rapot</th>
												<th rowspan="2">Deskripsi</th>
												<th rowspan="2">Aksi</th>
											</tr>
											<tr>
												<th>1</th>
												<th>2</th>
												<th>3</th>
												<th>4</th>
												<th>5</th>
												<th>1</th>
												<th>2</th>
												<th>3</th>
												<th>4</th>
												<th>5</th>
												<th>1</th>
												<th>2</th>
												<th>3</th>
												<th>4</th>
												<th>5</th>
												<th>1</th>
												<th>2</th>
												<th>3</th>
												<th>4</th>
												<th>5</th>
												<th>1</th>
												<th>2</th>
												<th>3</th>
												<th>4</th>
												<th>5</th>
												<th>1</th>
												<th>2</th>
												<th>3</th>
												<th>4</th>
												<th>5</th>
												<th>1</th>
												<th>2</th>
												<th>3</th>
												<th>4</th>
												<th>5</th>
												<th>4.1</th>
												<th>4.2</th>
												<th>4.3</th>
												<th>4.4</th>
												<th>4.5</th>
												<th>4.6</th>
												<th>4.7</th>
											</tr>
										</thead>
										<tbody>
											<?php foreach ($mapel_list as $key => $value) : ?>
												<form action="<?= base_url() ?>siswa/keterampilan/add/" method="post" enctype="multipart/form-data">
													<?php $keterampilans = (array) $keterampilan->list_keterampilan($siswa->id_siswa, $id_semester, $value->id_mapel); ?>
													<tr>
														<td><?= $value->nama_mapel ?> <?= $value->nama_kelompok ?>
															<input type="hidden" name="i_siswa" value="<?= 0 ?>"></td>
														<input type="hidden" name="i_semester" value="<?php echo $id_semester ?>">
														<?php if (count($keterampilans) > 0) { ?>
															<td><input type="number" name="41_1" style="width: 50px;" value="<?php echo $keterampilans['41_1'] ?>" readonly="readonly"></td>
															<td><input type="number" name="41_2" style="width: 50px;" value="<?php echo $keterampilans['41_2'] ?>" readonly="readonly"></td>
															<td><input type="number" name="41_3" style="width: 50px;" value="<?php echo $keterampilans['41_3'] ?>" readonly="readonly"></td>
															<td><input type="number" name="41_4" style="width: 50px;" value="<?php echo $keterampilans['41_4'] ?>" readonly="readonly"></td>
															<td><input type="number" name="41_5" style="width: 50px;" value="<?php echo $keterampilans['41_5'] ?>" readonly="readonly"></td>
															<td><input type="number" name="42_1" style="width: 50px;" value="<?php echo $keterampilans['42_1'] ?>" readonly="readonly"></td>
															<td><input type="number" name="42_2" style="width: 50px;" value="<?php echo $keterampilans['42_2'] ?>" readonly="readonly"></td>
															<td><input type="number" name="42_3" style="width: 50px;" value="<?php echo $keterampilans['42_3'] ?>" readonly="readonly"></td>
															<td><input type="number" name="42_4" style="width: 50px;" value="<?php echo $keterampilans['42_4'] ?>" readonly="readonly"></td>
															<td><input type="number" name="42_5" style="width: 50px;" value="<?php echo $keterampilans['42_5'] ?>" readonly="readonly"></td>
															<td><input type="number" name="43_1" style="width: 50px;" value="<?php echo $keterampilans['43_1'] ?>" readonly="readonly"></td>
															<td><input type="number" name="43_2" style="width: 50px;" value="<?php echo $keterampilans['43_2'] ?>" readonly="readonly"></td>
															<td><input type="number" name="43_3" style="width: 50px;" value="<?php echo $keterampilans['43_3'] ?>" readonly="readonly"></td>
															<td><input type="number" name="43_4" style="width: 50px;" value="<?php echo $keterampilans['43_4'] ?>" readonly="readonly"></td>
															<td><input type="number" name="43_5" style="width: 50px;" value="<?php echo $keterampilans['43_5'] ?>" readonly="readonly"></td>
															<td><input type="number" name="44_1" style="width: 50px;" value="<?php echo $keterampilans['44_1'] ?>" readonly="readonly"></td>
															<td><input type="number" name="44_2" style="width: 50px;" value="<?php echo $keterampilans['44_2'] ?>" readonly="readonly"></td>
															<td><input type="number" name="44_3" style="width: 50px;" value="<?php echo $keterampilans['44_3'] ?>" readonly="readonly"></td>
															<td><input type="number" name="44_4" style="width: 50px;" value="<?php echo $keterampilans['44_4'] ?>" readonly="readonly"></td>
															<td><input type="number" name="44_5" style="width: 50px;" value="<?php echo $keterampilans['44_5'] ?>" readonly="readonly"></td>
															<td><input type="number" name="45_1" style="width: 50px;" value="<?php echo $keterampilans['45_1'] ?>" readonly="readonly"></td>
															<td><input type="number" name="45_2" style="width: 50px;" value="<?php echo $keterampilans['45_2'] ?>" readonly="readonly"></td>
															<td><input type="number" name="45_3" style="width: 50px;" value="<?php echo $keterampilans['45_3'] ?>" readonly="readonly"></td>
															<td><input type="number" name="45_4" style="width: 50px;" value="<?php echo $keterampilans['45_4'] ?>" readonly="readonly"></td>
															<td><input type="number" name="45_5" style="width: 50px;" value="<?php echo $keterampilans['45_5'] ?>" readonly="readonly"></td>
															<td><input type="number" name="46_1" style="width: 50px;" value="<?php echo $keterampilans['46_1'] ?>" readonly="readonly"></td>
															<td><input type="number" name="46_2" style="width: 50px;" value="<?php echo $keterampilans['46_2'] ?>" readonly="readonly"></td>
															<td><input type="number" name="46_3" style="width: 50px;" value="<?php echo $keterampilans['46_3'] ?>" readonly="readonly"></td>
															<td><input type="number" name="46_4" style="width: 50px;" value="<?php echo $keterampilans['46_4'] ?>" readonly="readonly"></td>
															<td><input type="number" name="46_5" style="width: 50px;" value="<?php echo $keterampilans['46_5'] ?>" readonly="readonly"></td>
															<td><input type="number" name="47_1" style="width: 50px;" value="<?php echo $keterampilans['47_1'] ?>" readonly="readonly"></td>
															<td><input type="number" name="47_2" style="width: 50px;" value="<?php echo $keterampilans['47_2'] ?>" readonly="readonly"></td>
															<td><input type="number" name="47_3" style="width: 50px;" value="<?php echo $keterampilans['47_3'] ?>" readonly="readonly"></td>
															<td><input type="number" name="47_4" style="width: 50px;" value="<?php echo $keterampilans['47_4'] ?>" readonly="readonly"></td>
															<td><input type="number" name="47_5" style="width: 50px;" value="<?php echo $keterampilans['47_5'] ?>" readonly="readonly"></td>
															<td><input type="number" name="41" style="width: 50px;" value="<?php echo $keterampilans['41_'] ?>" readonly="readonly"></td>
															<td><input type="number" name="42" style="width: 50px;" value="<?php echo $keterampilans['42_'] ?>" readonly="readonly"></td>
															<td><input type="number" name="43" style="width: 50px;" value="<?php echo $keterampilans['43_'] ?>" readonly="readonly"></td>
															<td><input type="number" name="44" style="width: 50px;" value="<?php echo $keterampilans['44_'] ?>" readonly="readonly"></td>
															<td><input type="number" name="45" style="width: 50px;" value="<?php echo $keterampilans['45_'] ?>" readonly="readonly"></td>
															<td><input type="number" name="46" style="width: 50px;" value="<?php echo $keterampilans['46_'] ?>" readonly="readonly"></td>
															<td><input type="number" name="47" style="width: 50px;" value="<?php echo $keterampilans['47_'] ?>" readonly="readonly"></td>
															<td><input type="number" name="nilai_rapot" style="width: 50px;" value="<?php echo $keterampilans['nilai_rapot'] ?>" readonly="readonly"></td>
															<td><textarea name="deskripsi" id="deskripsi" readonly="readonly"><?php echo $keterampilans['deskripsi'] ?></textarea></td>
														<?php } else { ?>
															<td><input type="number" name="41_1" style="width: 50px;" readonly="readonly"></td>
															<td><input type="number" name="41_2" style="width: 50px;" readonly="readonly"></td>
															<td><input type="number" name="41_3" style="width: 50px;" readonly="readonly"></td>
															<td><input type="number" name="41_4" style="width: 50px;" readonly="readonly"></td>
															<td><input type="number" name="41_5" style="width: 50px;" readonly="readonly"></td>
															<td><input type="number" name="42_1" style="width: 50px;" readonly="readonly"></td>
															<td><input type="number" name="42_2" style="width: 50px;" readonly="readonly"></td>
															<td><input type="number" name="42_3" style="width: 50px;" readonly="readonly"></td>
															<td><input type="number" name="42_4" style="width: 50px;" readonly="readonly"></td>
															<td><input type="number" name="42_5" style="width: 50px;" readonly="readonly"></td>
															<td><input type="number" name="43_1" style="width: 50px;" readonly="readonly"></td>
															<td><input type="number" name="43_2" style="width: 50px;" readonly="readonly"></td>
															<td><input type="number" name="43_3" style="width: 50px;" readonly="readonly"></td>
															<td><input type="number" name="43_4" style="width: 50px;" readonly="readonly"></td>
															<td><input type="number" name="43_5" style="width: 50px;" readonly="readonly"></td>
															<td><input type="number" name="44_1" style="width: 50px;" readonly="readonly"></td>
															<td><input type="number" name="44_2" style="width: 50px;" readonly="readonly"></td>
															<td><input type="number" name="44_3" style="width: 50px;" readonly="readonly"></td>
															<td><input type="number" name="44_4" style="width: 50px;" readonly="readonly"></td>
															<td><input type="number" name="44_5" style="width: 50px;" readonly="readonly"></td>
															<td><input type="number" name="45_1" style="width: 50px;" readonly="readonly"></td>
															<td><input type="number" name="45_2" style="width: 50px;" readonly="readonly"></td>
															<td><input type="number" name="45_3" style="width: 50px;" readonly="readonly"></td>
															<td><input type="number" name="45_4" style="width: 50px;" readonly="readonly"></td>
															<td><input type="number" name="45_5" style="width: 50px;" readonly="readonly"></td>
															<td><input type="number" name="46_1" style="width: 50px;" readonly="readonly"></td>
															<td><input type="number" name="46_2" style="width: 50px;" readonly="readonly"></td>
															<td><input type="number" name="46_3" style="width: 50px;" readonly="readonly"></td>
															<td><input type="number" name="46_4" style="width: 50px;" readonly="readonly"></td>
															<td><input type="number" name="46_5" style="width: 50px;" readonly="readonly"></td>
															<td><input type="number" name="47_1" style="width: 50px;" readonly="readonly"></td>
															<td><input type="number" name="47_2" style="width: 50px;" readonly="readonly"></td>
															<td><input type="number" name="47_3" style="width: 50px;" readonly="readonly"></td>
															<td><input type="number" name="47_4" style="width: 50px;" readonly="readonly"></td>
															<td><input type="number" name="47_5" style="width: 50px;" readonly="readonly"></td>
															<td><input type="number" name="41" style="width: 50px;" readonly="readonly"></td>
															<td><input type="number" name="42" style="width: 50px;" readonly="readonly"></td>
															<td><input type="number" name="43" style="width: 50px;" readonly="readonly"></td>
															<td><input type="number" name="44" style="width: 50px;" readonly="readonly"></td>
															<td><input type="number" name="45" style="width: 50px;" readonly="readonly"></td>
															<td><input type="number" name="46" style="width: 50px;" readonly="readonly"></td>
															<td><input type="number" name="47" style="width: 50px;" readonly="readonly"></td>
															<td><input type="number" name="nilai_rapot" style="width: 50px;" readonly="readonly"></td>
															<td><textarea name="deskripsi" id="deskripsi" readonly="readonly"></textarea></td>
														<?php } ?>
														<td><button type="submit" class="btn btn-accent">Simpan</button></td>
													</tr>
												</form>
											<?php endforeach ?>
										</tbody>
									</table>
								</div>

							</div>
							<!-- <button type="submit" class="btn btn-accent"><i class="fa fa-save"></i> Simpan</button>
								<button type="reset" class="btn btn-danger"><i class="fa fa-refresh"></i> Reset</button> -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	function filter() {
		var id_semester = $('#id_semester').val();
		var url = '<?php echo base_url() ?>siswa/keterampilan/list/' + id_semester;
		location.href = url;
	}
</script>